
import React, { useState, useEffect } from "react";
import  Link  from "next/link";
import axios from "axios";
import { setToast } from "../components/ToastMsg";


import Navbar from '../components/NavbarComp'

import CircularProgress from "@material-ui/core/CircularProgress";
import Cookies from "js-cookie";


export default function Login() {




    const [first, setFirst] = useState("");
    const [last, setLast] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirm, setConfirm] = useState("");
    const [error, setError] = useState(null);
    const [load, setload] = useState(false);


    let handleSignin = (e) => {
        setload(true);
        e.preventDefault();
        let user = {
            first,
            last,
            email,
            password,
            confirm
        };
        axios
            .post("/user/signup", user)
            .then((res) => {
                //console.log(res.data);
                if (res.status === 200) {
                    //Cookies.set("flikhs_auth", res.data.token);
                    //localStorage.setItem('auth_token1', res.data.token)
                    // dispatch({type:"SET_USER", payload: res.data.user})
                    // dispatch({type:"SET_PROFILE", payload: res.data.user})
                    setToast("Register success", "success");
                    setload(false);
                    window.location.pathname = '/login'
                }
            })
            .catch((err) => {
                err.response && setError(err.response.data);
                setload(false);
            });
    };


    return (
        <>
            <Navbar />

            <div className="container">

                <div className="login_container">
                    <div className="row login_main_content bg-success text-center">
                        <div className="col-md-4 text-center company__info">
                            <span className="company__logo">
                                <img src="/logo.png"></img>
                            </span>
                            {/* <h4 className="company_title">Your Company Logo</h4> */}
                        </div>
                        <div className="col-md-8 col-xs-12 col-sm-12 login_form ">
                            <div className="container-fluid">
                                <div className="row">
                                    <h2 className="mt-2">Register</h2>
                                </div>
                                {error && error.error ? (
                                    <p style={{ color: "red" }}>{error.error}</p>
                                ) : null}
                                <div className="row">
                                    <form onSubmit={handleSignin} className="form-group">
                                        <div className="row mb-3">
                                            <div className="col-6">
                                                <input
                                                    onChange={(e) => setFirst(e.target.value)}
                                                    type="first"
                                                    name="first"
                                                    id="first"
                                                    value={first}
                                                    className={`form-control form__input ${error && error.first && "is-invalid"}`}
                                                    placeholder="First Name"
                                                />
                                                {
                                                    error && error.first && <div className="invalid-feedback">{error.first}</div>
                                                }
                                            </div>
                                            <div className="col-6">
                                                <input
                                                    onChange={(e) => setLast(e.target.value)}
                                                    type="last"
                                                    name="last"
                                                    id="last"
                                                    value={last}
                                                    className={`form-control form__input ${error && error.last && "is-invalid"}`}
                                                    placeholder="Last Name"
                                                />
                                                {
                                                    error && error.last && <div className="invalid-feedback">{error.last}</div>
                                                }

                                            </div>

                                        </div>
                                        <div className="row mb-3">
                                            <input
                                                onChange={(e) => setEmail(e.target.value)}
                                                type="email"
                                                name="email"
                                                id="email"
                                                value={email}
                                                className={`form-control form__input ${error && error.email && "is-invalid"}`}
                                                placeholder="Email"
                                            />
                                            {
                                                error && error.email && <div className="invalid-feedback">{error.email}</div>
                                            }
                                        </div>
                                        <div className="row">
                                            {/* <!-- <span className="fa fa-lock"></span> --> */}
                                            <input
                                                onChange={(e) => setPassword(e.target.value)}
                                                type="password"
                                                name="password"
                                                id="password"
                                                value={password}
                                                className={`form-control form__input ${error && error.password && "is-invalid"}`}
                                                placeholder="Password"
                                            />
                                            {
                                                error && error.password && <div className="invalid-feedback">{error.password}</div>
                                            }
                                        </div>
                                        <div className="row">
                                            {/* <!-- <span className="fa fa-lock"></span> --> */}
                                            <input
                                                onChange={(e) => setConfirm(e.target.value)}
                                                type="confirm"
                                                name="confirm"
                                                id="confirm"
                                                value={confirm}
                                                className={`form-control form__input ${error && error.confirm && "is-invalid"}`}
                                                placeholder="Confirm password"
                                            />
                                            {
                                                error && error.confirm && <div className="invalid-feedback">{error.confirm}</div>
                                            }
                                        </div>
                                        {/* <div className="row">
								<input type="checkbox" name="remember_me" id="remember_me" className="" />
								<label for="remember_me">Remember Me!</label>
							</div> */}
                                        <div className="row">
                                            <button type="submit" className="btn" >{
                                                load ? <CircularProgress color="white" size={30} /> : 'Register'
                                            } </button>
                                        </div>
                                    </form>
                                </div>
                                <div className="row py-4">
                                    <p>
                                        Already have an account? <Link href="/login"><a >Login Here</a></Link>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    );
}
